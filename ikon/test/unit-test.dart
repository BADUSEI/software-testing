import 'package:flutter_test/flutter_test.dart';
import 'package:ikon/src/validation_mixin.dart';

// Unit tests
void main() {

  test("email without '@' returns error string", (){
    var result = ValidationMixin().validateEmail('ikon');
    expect(result, 'Pls enter a valid email address');
  });

  test("email '@' returns null", (){
    var result = ValidationMixin().validateEmail('ikon@gmail.com');
    expect(result, null);
  });

  test('empty password', (){
    var result = ValidationMixin().validatePassword('');
    expect(result, 'Password must be at least 4 characters');
  });

  test("password less than 4 characters", (){
    var result = ValidationMixin().validatePassword('ijn');
    expect(result, 'Password must be at least 4 characters');
  });

  test("password exceeding 4 charters", (){
    var result = ValidationMixin().validatePassword('ikon');
    expect(result, null);
  });

}